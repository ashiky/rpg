import Assassin from "./race/assassin.js"
import Berzerk from "./race/berzerk.js"
import Dragon from "./race/dragon.js"
import Elfe from "./race/elfe.js"
import Golem from "./race/golem.js"
import Grifon from "./race/grifon.js"
import Humain from "./race/humain.js"
import LoupGarou from "./race/loupGarou.js"
import Nain from "./race/nain.js"
let nain = new Nain("Gimli",2000,2000,100,1,8);
let elfe = new Elfe("Legolas",300,300,100,1,0);
let humain = new Humain("Aragorn",300,300,100,1,0);
let assassin = new Assassin("Valeera",300,300,100,1,0);
let berzerk = new Berzerk("Khorne",300,300,100,1,0);
let dragon = new Dragon("Smog",300,300,100,1,0);
let golem = new Golem("Caillou",300,300,100,1,0);
let grifon = new Grifon("Felstad",300,300,100,1,0);
let loupGarou = new LoupGarou("Felix",300,300,100,1,0);

let ennemi =[assassin,berzerk,grifon,golem,loupGarou,dragon]

for(let i=0;i<ennemi.length;i++){
    while(!nain.mort() && !ennemi[i].mort() ){
        console.log(nain.getPvActuel());
        console.log(dragon.getPvActuel());
        nain.attaque(ennemi[i]);
        ennemi[i].attaque(nain);
    }    
}
