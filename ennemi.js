import Personnage from "../personnage.js"
export default class Ennemi extends Personnage {
    #volant;
    constructor(nom, pv, pvActuel, force, niveau, experience, race, volant) {
        super(nom, pv, pvActuel, force, niveau, race, experience)
        this.#volant = volant
    }
    getVolant() {
        return this.#volant;
    }

    envole() {
        if (this.getVolant() == "sol") {
            console.log(this.getVolant());
            return this.#volant = "ciel"
        } else {
            console.log(this.getVolant());
            return this.#volant = "sol"
        }
    }
    attaqueDepuisCiel(cible) {
        if (cible.etreAttaque(super.getForce() * 1.1)) {
        }
    }
}