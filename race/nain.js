import Hero from "../hero.js";
export default class Nain extends Hero {
    constructor(nom, pv, pvActuel, force, niveau, experience) {
        super(nom, pv, pvActuel, force, niveau, experience, "Nain")
    }
    etreAttaque(degat) {
        let numAleatoire = Math.random(10);
        if (parseInt(numAleatoire * 10) <= 2) {
            return super.etreAttaque(degat / 2);
        } else {
            return super.etreAttaque(degat);
        }
    }
}




