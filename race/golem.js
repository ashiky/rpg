import Ennemi from "../ennemi.js"
export default class Golem extends Ennemi{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Golem","sol")
    }
    etreAttaque(degat) {
        let numAleatoire =Math.random(10);
        console.log(parseInt(numAleatoire * 10));
        if( parseInt(numAleatoire * 10) <= 5){
            return super.etreAttaque(0);
        }else {
            return super.etreAttaque(degat);
        }
    }
}