import Ennemi from "../ennemi.js"
export default class LoupGarou extends Ennemi{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Loup-Garoup","sol")
    }
    etreAttaque(degat) {
        return super.etreAttaque(degat / 2);
}
}
