let multiplilleur = 1;
import Ennemi from "../ennemi.js";
export default class Assassin extends Ennemi{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Assassin","sol");
    }
    attaque (cible){
        multiplilleur += 0.1;
        cible.etreAttaque(this.getForce()*multiplilleur);
    }
}
