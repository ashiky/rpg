import Hero from "../hero.js"
export default class Humain extends Hero{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Humain")
    }
    attaque(cible){
        if(cible.getVolant() == "sol"){
            cible.etreAttaque(this.getForce()*1.1);
        } else {
            cible.etreAttaque(this.getForce()*0.9);
        }
    }
}
