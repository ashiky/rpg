import Hero from "../hero.js"
export default class Elfe extends Hero{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Elfe")
    }
    attaque(cible){
        if(cible.getVolant() == "ciel"){
            cible.etreAttaque(this.getForce()*1.1);
        } else {
            cible.etreAttaque(this.getForce()*0.9);
        }
    }
}
