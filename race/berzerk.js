import Ennemi from "../ennemi.js"
export default class Berzerk extends Ennemi{  
    constructor(nom,pv,pvActuel,force,niveau,experience){
        super(nom,pv,pvActuel,force,niveau,experience,"Berzerk","sol")
    }
    etreAttaque(degat) {
        return super.etreAttaque(degat / (0.3));
}
}