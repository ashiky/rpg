let decompte = 0;
import Ennemi from "../ennemi.js"
export default class Dragon extends Ennemi {
    constructor(nom, pv, pvActuel, force, niveau,volant ,experience) {
        super(nom, pv, pvActuel, force, niveau, experience,volant,"sol")
    }

    etreAttaque(degat) {
        if(this.getVolant() == "ciel") {
            return super.etreAttaque((degat * 0.9 )/ 2);
        } else {
            return super.etreAttaque(degat / 2);
        }
    }
    attaque(cible){
        if(decompte == 0){
            decompte = 1;
            console.log("normal");
            super.attaque(cible);
            decompte = 1;
        } else if (decompte == 1){
            console.log("envole");
            this.envole();
            decompte = 2;
        } else {
            console.log("attaque ciel");
            this.attaqueDepuisCiel(cible)
            this.envole();
            decompte = 0
        }
    }
}
