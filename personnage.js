export default class Personnage {
    #nom;
    #pv;
    #pvActuel;
    #force;
    #niveau;
    #experience;
    #race;
    constructor(nom, pv, pvActuel, force, niveau, experience, race) {
        this.#nom = nom;
        this.#pv = pv;
        this.#pvActuel = pvActuel;
        this.#force = force;
        this.#niveau = niveau;
        this.#experience = experience;
        this.#race = race;

    }
    getNom() {
        return this.#nom;
    }
    getPv() {
        return parseInt(this.#pv);
    }
    getPvActuel() {
        return parseInt(this.#pvActuel);
    }
    getForce() {
        return this.#force;
    }
    getNiveau() {
        return this.#niveau;
    }
    getExperience() {
        return this.#experience;
    }
    getRace() {
        return this.#race;
    }

    mort() {
        if (this.#pvActuel <= 0) {
            return true;
        }
    }
    etreAttaque(degat) {
        this.#pvActuel -= degat;
        if (this.mort()) {
            this.#pvActuel = 0;
            console.log(this.getNom() + " est mort(e).")
            return true;
        }
        else {
            console.log("Il reste " + this.getPvActuel() + " PV " + this.#nom);
        }
    }
    attaque(cible) {
        if (cible.etreAttaque(this.#force)) {
            this.gagnerCombat(cible);
        }
    }
    gagnerCombat(cible) {
        this.#pv += (10 / 100) * cible.getPv();
        this.#pvActuel += (10 / 100) * cible.getPv();
        this.gagnerXP();
    }
    gagnerXP() {
        this.#experience += 2;
        if (this.#experience >= 10) {
            this.gagnerNiveau();
        }
    }
    gagnerNiveau() {
        this.#niveau += 1;
        this.#experience = 0;
    }
}

